import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

import 'fabric';
declare const fabric: any;

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  private canvas: any;
  width = 330;
  height = 400;
  zoomStartScale;
  constructor(public navCtrl: NavController) {

  }

  ionViewDidLoad() {
    this.canvas = new fabric.Canvas('canvas', {
      hoverCursor: 'pointer',
      selection: false,
      selectionBorderColor: 'blue'
    });

    this.canvas.setWidth(this.width);
    this.canvas.setHeight(this.height);

    this.setCanvasImage('assets/imgs/bakgrund.png');

    this.canvas.on('mouse:wheel', function (opt) {
      var delta = opt.e.deltaY;
      var zoom = this.getZoom();
      zoom = zoom + delta / 200;
      if (zoom > 20) zoom = 20;
      if (zoom < 0.01) zoom = 0.01;
      this.setZoom(zoom);
      opt.e.preventDefault();
      opt.e.stopPropagation();
    })

    var pausePanning = false;
    var zoomStartScale;

    this.canvas.on({
      'touch:gesture': function (e) {

        if (e.e.touches && e.e.touches.length == 2) {
          pausePanning = true;
          var point = new fabric.Point(e.self.x, e.self.y);
          if (e.self.state == "start") {
            zoomStartScale = this.getZoom();
          }
          var delta = zoomStartScale * e.self.scale;
          this.zoomToPoint(point, delta);
          pausePanning = false;
        }
      },
      'object:selected': function () {
        pausePanning = true;
      },
      'selection:cleared': function () {
        pausePanning = false;
      },
      'touch:drag': function (e) {
        if (pausePanning == false && undefined != e.self.x && undefined != e.self.x) {
          // currentX = e.self.x;
          // currentY = e.self.y;
          // xChange = currentX - lastX;
          // yChange = currentY - lastY;

          // if( (Math.abs(currentX - lastX) <= 50) && (Math.abs(currentY - lastY) <= 50)) {
          //     var delta = new fabric.Point(xChange, yChange);
          //     canvas.relativePan(delta);
          // }

          // lastX = e.self.x;
          // lastY = e.self.y;
        }
      }
    });
  }




  addFordon() {
    let regnr =
      this.addFigure('rectangle');
  }

  addFigure(figure) {
    let add: any;
    switch (figure) {
      case 'rectangle':
        add = new fabric.Rect({
          width: 200, height: 100, left: 10, top: 10, angle: 0,
          fill: '#3f51b5'
        });
        break;
      case 'square':
        add = new fabric.Rect({
          width: 100, height: 100, left: 10, top: 10, angle: 0,
          fill: '#4caf50'
        });
        break;
      case 'triangle':
        add = new fabric.Triangle({
          width: 100, height: 100, left: 10, top: 10, fill: '#2196f3'
        });
        break;
      case 'circle':
        add = new fabric.Circle({
          radius: 50, left: 10, top: 10, fill: '#ff5722'
        });
        break;
    }
    this.extend(add, this.randomId());
    this.canvas.add(add);
    this.selectItemAfterAdded(add);
  }

  extend(obj, id) {
    obj.toObject = (function (toObject) {
      return function () {
        return fabric.util.object.extend(toObject.call(this), {
          id: id
        });
      };
    })(obj.toObject);
  }

  randomId() {
    return Math.floor(Math.random() * 999999) + 1;
  }

  selectItemAfterAdded(obj) {
    // this.canvas.().renderAll();
    this.canvas.setActiveObject(obj);
  }

  addImage(url) {
    if (url) {
      fabric.Image.fromURL(url, (image) => {
        image.set({
          left: 10,
          top: 10,
          angle: 0,
          padding: 10,
          cornersize: 10,
          hasRotatingPoint: true
        });
        image.width = 200;
        image.height = 200;
        image.scaleX = 0.5;
        image.scaleY = 0.5;
        this.extend(image, this.randomId());
        this.canvas.add(image);
        this.selectItemAfterAdded(image);
      });
    }
  }

  setCanvasImage(url) {
    let self = this;
    if (url) {
      this.canvas.setBackgroundImage(url, self.canvas.renderAll.bind(this.canvas),
        {
          left: -100,
          top: -100,
          originX: 'left',
          originY: 'top',
        });
    }
  }
}
