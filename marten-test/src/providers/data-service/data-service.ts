
import { Injectable } from '@angular/core';
import {Fordon} from "../../pages/fordon";

/*
  Generated class for the DataServiceProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class DataServiceProvider {

  private fordonList: Fordon[] = [];
  private canvas: string;
  private carsOnCanvas: number = 0;

  constructor() {
    // temp
    this.fordonList.push(new Fordon('abc123', 'Adam Adamsson', 'A'));
    this.carsOnCanvas = this.fordonList.length;
  }

  public getFordonList() {
    return this.fordonList;
  }

  public setFordonList(list: Fordon[]) {
    this.fordonList = list;
  }

  public saveCanvas(canvasJson: string) {
    this.canvas = canvasJson;
  }

  public getCanvasJSON(): string {
    return this.canvas;
  }

  public saveNumberOfCarsAddedToCanvas(nrOfCarsOnCanvas: number) {
    this.carsOnCanvas = nrOfCarsOnCanvas;
  }

  public getCarsNotAddedToCanvas() {
    let list = [];
    if (this.carsOnCanvas < this.fordonList.length) {
      for (let i = this.carsOnCanvas; i < this.fordonList.length; i++) {
        list.push(this.fordonList[i]);
      }
    }
    console.log(list);
    return list;
  }

}
