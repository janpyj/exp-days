import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PlaceFordonPage } from './place-fordon';

@NgModule({
  declarations: [
    PlaceFordonPage,
  ],
  imports: [
    IonicPageModule.forChild(PlaceFordonPage),
  ],
})
export class PlaceFordonPageModule {}
