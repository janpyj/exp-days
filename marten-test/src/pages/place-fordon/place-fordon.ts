import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {DataServiceProvider} from "../../providers/data-service/data-service";
import {Fordon} from "../fordon";

import 'fabric';
declare const fabric: any;

/**
 * Generated class for the PlaceFordonPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-place-fordon',
  templateUrl: 'place-fordon.html',
})
export class PlaceFordonPage {

  CAR_ICON_URL = 'assets/imgs/car_top.png';
  ARROW_DOWN_URL = 'assets/imgs/arrow_down.png';
  DOG_ICON_URL = 'assets/imgs/dog.png';

  fordonList: Fordon[];
  objects: any[] = [];

  private canvas: any;
  private size: any = {
    width: 340,
    height: 400
  };

  constructor(public navCtrl: NavController, public navParams: NavParams, public dataService: DataServiceProvider) {
  }

  ionViewDidEnter() {
    console.log('ionViewDidEnter PlaceFordonPage');

    this.fordonList = this.dataService.getFordonList();
    this.size.width = window.innerWidth - 30;

    const canvasObjectJSON = this.dataService.getCanvasJSON();

    if (!Boolean(canvasObjectJSON)) {
      this.canvas = new fabric.Canvas('canvas', {
        hoverCursor: 'pointer',
        selection: true,
        selectionBorderColor: 'blue'
      });

      this.canvas.setBackgroundImage('assets/imgs/map-small.png', this.canvas.renderAll.bind(this.canvas), {
        backgroundImageOpacity: 0.5,
        backgroundImageStretch: false
      });

      // this.canvas.setBackgroundImage(background);

      this.canvas.setWidth(this.size.width);
      this.canvas.setHeight(this.size.height);

      if (Boolean(this.fordonList)) {
        this.fordonList.forEach((fordon, index) => {
          this.addCar(fordon, index);
        });
      }
    } else {
      this.canvas.loadFromJSON(canvasObjectJSON, () => {
        this.dataService.getCarsNotAddedToCanvas().forEach((fordon, index) => {
          this.addCar(fordon, index);
        });
      });
    }
  }

  ionViewDidLeave() {
    this.dataService.saveCanvas(JSON.stringify(this.canvas));
    this.dataService.saveNumberOfCarsAddedToCanvas(this.fordonList.length);
  }

  addCar(fordon: Fordon, index: number) {
    if (this.CAR_ICON_URL) {

      const left = (index * 10);

      fabric.Image.fromURL(this.CAR_ICON_URL, (image) => {
        image.set({
          angle: 90,
          padding: 10,
          cornersize: 10,
          hasRotatingPoint: true,
        });
        image.scale(0.2);
        this.extend(image, this.randomId());

        let textString = fordon.symbol;
        let text = new fabric.IText(textString, {
          left: -60,
          top: 40,
          fontFamily: 'helvetica',
          angle: 0,
          fill: '#ff0000',
          scaleX: 0.5,
          scaleY: 0.5,
          fontWeight: 'bold',
          hasRotatingPoint: true
        });
        this.extend(text, this.randomId());

        const group = new fabric.Group([ image, text ], {
          left: 10 + left,
          top: 10 + left,
          angle: 0
        });

        this.extend(group, this.randomId());
        this.canvas.add(group);
        this.objects.push(group);

        this.selectItemAfterAdded(group);
      });
    }
  }

  removeLatestObj() {
    const obj = this.objects.pop();
    this.canvas.remove(obj);
  }

  addDog() {
    this.addObject(this.DOG_ICON_URL);
  }

  addArrow() {
    this.addObject(this.ARROW_DOWN_URL);
  }

  addObject(url: string) {
    fabric.Image.fromURL(url, (image) => {
      image.set({
        angle: 0,
        left: (this.size.width/2)-25,
        top: (this.size.height/2)-25,
        padding: 10,
        cornersize: 10,
        hasRotatingPoint: true,
      });


      this.extend(image, this.randomId());
      this.canvas.add(image);
      this.objects.push(image);

      this.selectItemAfterAdded(image);
    });
  }

  extend(obj, id) {
    obj.toObject = (function (toObject) {
      return function () {
        return fabric.util.object.extend(toObject.call(this), {
          id: id
        });
      };
    })(obj.toObject);
  }

  randomId() {
    return Math.floor(Math.random() * 999999) + 1;
  }

  selectItemAfterAdded(obj) {
    // this.canvas.().renderAll();
    this.canvas.setActiveObject(obj);
  }

}
