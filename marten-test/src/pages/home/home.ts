import { Component } from '@angular/core';
import {AlertController, NavController} from 'ionic-angular';
import 'fabric';
declare const fabric: any;

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  private canvas: any;
 // private fordonList: Fordon[] = [];


  private size: any = {
    width: 340,
    height: 400
  };

  constructor(public navCtrl: NavController, private alertCtrl: AlertController) {
    this.size.width = window.innerWidth - 30;
  }

  ionViewDidLoad(){
    this.canvas = new fabric.Canvas('canvas', {
      hoverCursor: 'pointer',
      selection: true,
      selectionBorderColor: 'blue'
    });

    this.canvas.setWidth(this.size.width);
    this.canvas.setHeight(this.size.height);
  }

  addFordonPromt() {
    let alert = this.alertCtrl.create({
      title: 'Lägg till fordon',
      inputs: [
        {
          name: 'regnr',
          placeholder: 'Regnr'
        }
      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Spara',
          handler: data => {
            this.addFordon(data.regnr);
          }
        }
      ]
    });
    alert.present();
  }

  addFordon(regnr: string) {
    this.addImageOnCanvas('assets/imgs/car_top.png', regnr);
  }

  addFigure(figure) {
    let add: any;
    switch (figure) {
      case 'rectangle':
        add = new fabric.Rect({
          width: 100, height: 50, left: 10, top: 10, angle: 0,
          fill: '#3f51b5'
        });
        break;
      case 'square':
        add = new fabric.Rect({
          width: 100, height: 100, left: 10, top: 10, angle: 0,
          fill: '#4caf50'
        });
        break;
      case 'triangle':
        add = new fabric.Triangle({
          width: 100, height: 100, left: 10, top: 10, fill: '#2196f3'
        });
        break;
      case 'circle':
        add = new fabric.Circle({
          radius: 50, left: 10, top: 10, fill: '#ff5722'
        });
        break;
    }
    this.extend(add, this.randomId());
    this.canvas.add(add);
    this.selectItemAfterAdded(add);
  }

  addImageOnCanvas(url, regnr) {
    if (url) {
      fabric.Image.fromURL(url, (image) => {
        image.set({
          angle: 0,
          padding: 10,
          cornersize: 10,
          hasRotatingPoint: true,
        });
        image.scale(0.2);
        this.extend(image, this.randomId());
        //this.canvas.add(image);

        let textString = regnr;
        let text = new fabric.IText(textString, {
          left: 20,
          top: 40,
          fontFamily: 'helvetica',
          angle: 0,
          fill: '#ff0000',
          scaleX: 0.5,
          scaleY: 0.5,
          fontWeight: '',
          hasRotatingPoint: true
        });
        this.extend(text, this.randomId());

        const group = new fabric.Group([ image, text ], {
          left: 10,
          top: 10,
          angle: 0
        });

        this.extend(group, this.randomId());
        this.canvas.add(group);

        this.selectItemAfterAdded(group);

        //this.fordonList.push(new Fordon(new KeyValue(regnr, group), 'A'));
      });
    }
  }

  extend(obj, id) {
    obj.toObject = (function (toObject) {
      return function () {
        return fabric.util.object.extend(toObject.call(this), {
          id: id
        });
      };
    })(obj.toObject);
  }

  randomId() {
    return Math.floor(Math.random() * 999999) + 1;
  }

  selectItemAfterAdded(obj) {
    // this.canvas.().renderAll();
    this.canvas.setActiveObject(obj);
  }

}
