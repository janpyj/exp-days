import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {DataServiceProvider} from "../../providers/data-service/data-service";
import {Fordon} from "../fordon";

/**
 * Generated class for the AddFordonPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-add-fordon',
  templateUrl: 'add-fordon.html',
})
export class AddFordonPage {

  fordonList: Fordon[];
  regnr: string;
  forare: string;

  symbol = ['A', 'B', 'C', 'D']

  constructor(public navCtrl: NavController, public navParams: NavParams, public dataService: DataServiceProvider) {
    this.fordonList = this.dataService.getFordonList();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AddFordonPage');
  }

  ionViewDidLeave() {
    console.log('ionViewDidLeave AddFordonPage');
    this.dataService.setFordonList(this.fordonList);
  }

  addFordon() {
    this.fordonList.push(new Fordon(this.regnr, this.forare, this.getSymbol()));
    this.regnr = null;
    this.forare = null;
  }

  getSymbol() {
    return String.fromCharCode(97 + this.fordonList.length).toUpperCase();
  }

}
