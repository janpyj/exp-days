import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AddFordonPage } from './add-fordon';

@NgModule({
  declarations: [
    AddFordonPage,
  ],
  imports: [
    IonicPageModule.forChild(AddFordonPage),
  ],
})
export class AddFordonPageModule {}
