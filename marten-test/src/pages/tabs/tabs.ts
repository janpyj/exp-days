import { Component } from '@angular/core';

import {AddFordonPage} from "../add-fordon/add-fordon";
import {PlaceFordonPage} from "../place-fordon/place-fordon";

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {

  tab1Root = AddFordonPage;
  tab2Root = PlaceFordonPage;

  constructor() {

  }
}
